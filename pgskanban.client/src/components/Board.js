import React from 'react';
import List from './List';
import './Board.css';
import { BASE_URL } from '../constans';
import axios from 'axios';
import { withRouter } from 'react-router-dom';

export class Board extends React.Component {
    constructor() {
        super();
        this.state = {
            boardName: '',
            boardId: 0,
            boardData: [],
        }
    }

    componentDidMount() {
        axios.get(BASE_URL + '/board').then((response) =>   {
        console.log(response);
        this.setState({
            boardData: response.data.lists,
            boardName: response.data.name,
            boardId: response.data.id
        })
       
    }).catch(
        () => {
            this.props.history.push('/new');
        }
    );;
     
    }

    onChangeListName = (e) => {
        this.setState({ listName: e.target.value});
    };

    onClickList = (e) => {
        axios.post(BASE_URL + "/list", { 
            boardId: this.state.boardId, 
            name: this.state.listName
        })
        .then( response => {console.log(response);
            this.setState(
                prevState => {
                return {
                    boardData: [...prevState.boardData, response.data ],
                    newListName: ''
                }
                }
            )
        } )
    };

    renderLists = () => {
        return(
            this.state.boardData.map(
                (list) => <List key={list.id } boardId={this.state.boardId}
                 listId={list.id} listName={list.name} cards={[]} />
                )
        )
    }

    render() {
        return(
            <div>
                
                <div>
                    <h1>{this.state.boardName}</h1>
                    <button className="btn btn-info" onClick={ this.onClickList} 
                    disabled={ !this.state.listName}>Add new list</button>               
                    <input type="text" onChange={ this.onChangeListName} className="listName__input"/>
                </div>
                <div className="container-fluid">
                    <div className="row flex-row flex-nowrap">
                        {this.renderLists()}
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(Board)